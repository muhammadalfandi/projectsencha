Ext.define('MuhammadAlfandi.view.menu.Cart', {
    extend: 'Ext.Container',
    xtype: 'cart',
    requires: [
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'MuhammadAlfandi.store.Personnel',
    ],

    viewModel:{
        stores:{
            personnel:{
                type: 'personnel'
            }
        }
    },
    
    layout: 'fit',
    shadow: true,
    items: [
        {
            xtype: 'toolbar',
            docked: 'top',
            scrollable: {
                y: false
            },
            items:[
            {
                xtype: 'component',
            },
            {
                iconCls: 'x-fa fa-bars',
                text: 'Dialy Buy',
                flex: 3,
                handler: function() {
                    Ext.Viewport.toggleMenu('left');
                }
            }
        ],
        },
        {
        xtype: 'dataview',
        scrollable: 'y',
        itemTpl:[
            '<table style="border-spacing:5px; border-collapse:separate">'+
            '<tr><td rowspan= 5><img class="image" src="resources/{photo}" width=100></td></tr>' +
            '<tr><td><font size = 4><b>{nama}</b></font><br><font color = blue><b>Harga : Rp.{harga}</b></font></td></tr>' +
            '<tr><td><font size = 4>Jumlah : {jumlah}</font><br><font size = 3>Status : {status}</font></td></tr>' +
            '<tr><td><button class="button1">Beli</button></td></tr>'+
            '<hr>',
        ],

        bind: {
            store: '{personnel}'
        },

        listeners: {
            select : 'onItemSelected'
        },
    }],
});
