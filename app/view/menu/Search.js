Ext.define('MuhammadAlfandi.view.menu.Search', {
    extend: 'Ext.Container',
    xtype: 'search',
    requires: [
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'MuhammadAlfandi.store.Category',
        'Ext.field.Search',
    ],

    viewModel:{
        stores:{
            category:{
                type: 'category'
            }
        }
    },
    
    layout: 'fit',
    shadow: true,
    items: [
        {
            xtype: 'toolbar',
            docked: 'top',
            scrollable: {
                y: false
            },
            items:[
            {
                xtype: 'component',
            },
            {
                iconCls: 'x-fa fa-bars',
                text: 'Dialy Buy',
                flex: 3,
                handler: function() {
                    Ext.Viewport.toggleMenu('left');
                }
            }],
        },
        {
            docked: 'top',
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'spacer'
                },
                {
                    xtype: 'searchfield',
                    placeHolder: 'Search Category',
                    name: 'searchfield',
                    listeners: {
                        change: function( me, newValue, oldValue, e0pts){
                            personnelStore = Ext.getStore('category');
                            personnelStore.filter('kategori', newValue);
                        }
                    }
                },
                {
                    xtype: 'spacer'
                }
            ]
        },
        {
        xtype: 'dataview',
        scrollable: 'y',
        itemTpl: '<table style="border-spacing:5px; border-collapse:separate">'+
        '<tr><td rowspan= 5><img src="resources/{image}" width=200; height=150;></td></tr>' +
        '<tr><td><font size = 4><b>{kategori}</b></font></td></tr>' +
        '<hr>',

        bind: {
            store: '{category}'
        },
    }],
});