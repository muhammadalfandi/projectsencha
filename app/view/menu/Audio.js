Ext.define('MuhammadAlfandi.view.menu.Audio', {
    extend: 'Ext.Container',
    xtype: 'MyAudio1',
    requires: [
        'Ext.Audio'
    ],
    listeners: {
        hide: function () {
            try {
                var video = this.down('audio');
                video.fireEvent('hide');
            }
            catch (e) {
            }
        },
        show: function () {
            try {
                var video = this.down('audio');
                video.fireEvent('show');
            }
            catch (e) {
            }
        }
    },
    layout: Ext.os.is.Android ? {
        type: 'vbox',
        pack: 'center',
        align: 'center'
    } : 'fit',
    items: Ext.os.is.Android ? [
        {
            xtype: 'audio',
            cls: 'myAudio',
            url: 'resources/Audio.mp3',
            loop: true,
            width: '300',
            enableControls: true
        },
    ] : [
        {
            xtype: 'audio',
            cls: 'myAudio',
            url: 'resources/Audio.mp3',
            loop: true
        }
    ]
});