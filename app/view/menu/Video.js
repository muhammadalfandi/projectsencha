Ext.define('MuhammadAlfandi.view.menu.Video', {
    extend: 'Ext.Container',
    xtype: 'MyVideo1',
    requires: [
        'Ext.Video'
    ],
    layout: {
        type: 'vbox',
        pack: 'center',
        align: 'center' 
    },
    shadow: true,
    listeners: {
        hide: function () {
            try {
                var video = this.down('video');
                video.fireEvent('hide');
            }
            catch (e) {
            }
        },
        show: function () {
            try {
                var video = this.down('video');
                video.fireEvent('show');
            }
            catch (e) {
            }
        }
    },
    items: [{
        xtype: 'video',
        url: 'resources/videoplayback.mp4',
        loop: true,
        width: 400,
        height: 500,
        posterUrl: 'resources/cover.png'
    }]
});