Ext.define('MuhammadAlfandi.view.menu.Profile', {
    extend: 'Ext.form.Panel',
    xtype : 'profile',
    requires: [
        'Ext.SegmentedButton',
    ],
    controller: 'main',
    shadow: true,
    cls: 'demo-solid-background',
    items: [
        {          
            docked: 'top',
            xtype: 'toolbar',
            title: 'Profile',
            items :[
                {
                    xtype: 'button',
                    text: 'Back',
                    ui: 'action',
                    handler: function(){
                        Ext.getCmp('thisprofile').destroy();
                    }
                },
                {
                    xtype: 'spacer'
                },
                {
                    xtype: 'button',
                    text: 'Edit Profile',
                    ui: 'action',
                    style: 'background-color:blue;',
                    handler: 'onEdit',
                }
            ]
        },
        {
            html : '<table style="border-spacing:5px; color:black; margin-left: auto; margin-right: auto; border-collapse:separate">'+
            '<tr><td><img src="../../resources/user1.png" width=300; height= 300;></td></tr>' +
            '<tr><td><font size = 5><b>Nama   : Muhammad Alfandi</b></font><br></td></tr>' +
            '<tr><td><font size = 5><b>Email  : Alfandi@gmail.com</b></font><br></td></tr>' +
            '<tr><td><font size = 5><b>Status : Pembeli</b></font></td></tr>',
        }
    ],
});