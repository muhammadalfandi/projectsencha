Ext.define('MuhammadAlfandi.view.menu.Tree', {
    extend: 'Ext.grid.Tree',
    xtype: 'tree',
    requires: [
        'Ext.grid.plugin.MultiSelection',
        'MuhammadAlfandi.view.menu.TreeModel'
    ],

    
    cls: 'demo-solid-background',
    shadow: true,

    viewModel: {
        type: 'tree-list'
    },

    bind: '{navItems}'
});