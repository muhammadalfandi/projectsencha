Ext.define('MuhammadAlfandi.view.menu.Settings', {
    extend: 'Ext.Panel',
    xtype : 'settings',
    requires: [
        'Ext.SegmentedButton',
        'Ext.form.FieldSet',
        'Ext.field.Number',
        'Ext.field.Password',
        'Ext.field.Email',
        'Ext.field.Select',
        'Ext.field.Hidden',
        'Ext.field.Radio'
    ],

    shadow: true,
    id: 'basicform',
    items: [
        {          
            docked: 'top',
            xtype: 'toolbar',
            title: 'Settings',
            items :[
                {
                    xtype: 'button',
                    text: 'Back',
                    ui: 'action',
                    handler: function(){
                        Ext.getCmp('thissettings').destroy();
                    }
                }
            ]
        },
        {
            xtype: 'fieldset',
            id: 'fieldset1',
            title: '<br><br><font size = 5><center>Setting Profile</center></font>',
            defaults: {
                labelWidth: '35%'
            },
            items: [
                {
                    xtype: 'textfield',
                    name: 'name',
                    label: 'Name',
                    placeHolder: 'Username',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'passwordfield',
                    revealable: true,
                    name : 'password',
                    placeHolder: 'Password',
                    label: 'Password',
                    clearIcon: true
                },
                {
                    xtype: 'emailfield',
                    name: 'email',
                    label: 'Email',
                    placeHolder: 'Email',
                    clearIcon: true
                },
                {
                    xtype: 'textareafield',
                    name: 'bio',
                    label: 'Alamat'
                }
            ]
        },
        {
            xtype: 'container',
            defaults: {
                xtype: 'button',
                style: 'margin: 1em',
                flex: 1
            },
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    text: 'Submit',
                    ui: 'action',
                    style: 'background-color:green; margin:14px; color:white;',
                    handler: function(){
                        Ext.Msg.alert('Successfully', 'Data anda berhasil di edit', this)
                    }
                    
                },
                {   
                    text: 'Reset',
                    ui: 'action',
                    style: 'background-color:red; margin:14px; color:white;',
                    handle: function(){
                        Ext.getCmp('basicform').reset();
                    }
                }
            ]
        }
    ],
});