Ext.define('MuhammadAlfandi.view.menu.Help', {
    extend: 'Ext.Panel',
    xtype : 'help',
    requires: [
        'Ext.SegmentedButton'
    ],

    shadow: true,

    html: '<br><center><font size = 5><b>Dialy Buy</b></font></center><br>' +
          '<font size = 4><p><b>Ini adalah aplikasi jual beli barang harian. Aplikasi ini memudahkan kita untuk melakukan proses jual beli barang.</b></p></font>' +
          '<font size = 4><p><b>Terdapat tiga menu utama yaitu home, search, cart</b></p><br></font>' +
          '<font size = 4><p><b>1. Menu home digunakan untuk menampilkan barang harian yang tersedia.</b></p></font><br>' +
          '<font size = 4><p><b>2. Menu search digunakan untuk mencari barang berdasarkan kategori.</b></p></font><br>' +
          '<font size = 4><p><b>3. Menu cart untuk melihat barang yang ditambahkan dari menu home sebelumnya.</b></p></font>',
    items: [
        {          
            docked: 'top',
            xtype: 'toolbar',
            title: 'Help',
            items :[
                {
                    xtype: 'button',
                    text: 'Back',
                    ui: 'action',
                    handler: function(){
                        Ext.getCmp('thishelp').destroy();
                    }
                }
            ]
        }
    ],
});