Ext.define('MuhammadAlfandi.view.menu.DataView', {
    extend: 'Ext.Container',
    xtype: 'home',
    requires: [
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'Ext.carousel.Carousel',
        'MuhammadAlfandi.store.Personnel',
    ],

    viewModel:{
        stores:{
            personnel:{
                type: 'personnel'
            }
        }
    },
    
    layout: 'fit',
    shadow: true,
    items: [
        {
            xtype: 'toolbar',
            docked: 'top',
            scrollable: {
                y: false
            },
            items:[
            {
                xtype: 'component',
            },
            {
                iconCls: 'x-fa fa-bars',
                text: 'Dialy Buy',
                flex: 3,
                handler: function() {
                    Ext.Viewport.toggleMenu('left');
                },
            }],
        },
        {
            xtype: 'carousel',
            ui: 'light',
            items: [{
                html: '<img src="resources/foto1.png" width=400; height=250; class="center">'
            },
            {
                html: '<img src="resources/foto2.jpg" width=400; height=250; class="center">'
            },
            {
                html: '<img src="resources/foto3.png" width=400; height=250; class="center">'
            }]
        },
        {
        xtype: 'dataview',
        scrollable: 'y',
 
        itemTpl:[
            '<table style="border-spacing:5px; border-collapse:separate">'+
            '<tr><td rowspan= 5><img class="image" src="resources/{photo}" width=100></td></tr>' +
            '<tr><td><font size = 4><b>{nama}</b></font><br><font color = blue><b>Harga : Rp.{harga}</b></font></td></tr>' +
            '<tr><td><font size = 4>Jumlah : {jumlah}</font><br><font size = 3>Status : {status}</font></td></tr>' +
            '<tr><td><button class="button2">Tambahkan Ke Keranjang</button></td></tr>'+
            '<hr>',
        ],

        listeners: {
            select : 'keranjang'
        },

        bind: {
            store: '{personnel}'
        },
    }],

    shadow: true,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults: {
        cls: 'demo-solid-background',
        flex: 1
    },

    initialize: function() {
        Ext.Viewport.setMenu(this.getMenuCfg('left'), {
            side: 'left',
            reveal: true
        });
    },

    doDestroy: function() {
        Ext.Viewport.removeMenu('left');
        this.callParent();
    },

    getMenuCfg: function(side) {
        return {
            items: [{
                text: 'Profile',
                iconCls: 'x-fa fa-user',
                scope: this,
                handler: function() {
                    Ext.Viewport.hideMenu(side);
                    if (!Ext.getCmp('thisprofile')) {
                        this.overlay = Ext.Viewport.add({
                            xtype: 'profile',                           
                            id: 'thisprofile',
                            showAnimation: {
                                type: 'popIn',
                                duration: 250,
                                easing: 'ease-out'
                            },
                            hideAnimation: {
                                type: 'popOut',
                                duration: 250,
                                easing: 'ease-out'
                            },
                            centered: true,
                            width: "100%",
                            height: "100%",
                            scrollable: true
                        });
                    }
                    this.overlay.show();
                }
            }, {
                text: 'Help',
                iconCls: 'x-fa fa-info-circle',
                scope: this,
                handler: function() {
                    Ext.Viewport.hideMenu(side);
                    if (!Ext.getCmp('thishelp')) {
                        this.overlay = Ext.Viewport.add({
                            xtype: 'help',
                            id : 'thishelp',
                            showAnimation: {
                                type: 'popIn',
                                duration: 250,
                                easing: 'ease-out'
                            },
                            hideAnimation: {
                                type: 'popOut',
                                duration: 250,
                                easing: 'ease-out'
                            },
                            centered: true,
                            width: "100%",
                            height: "100%",
                            scrollable: true
                        });
                    }
                    this.overlay.show();
                }
            }, {
                xtype: 'button',
                text: 'Settings',
                iconCls: 'x-fa fa-gear',
                scope: this,
                handler: function() {
                    Ext.Viewport.hideMenu(side);
                    if (!Ext.getCmp('thissettings')) {
                        this.overlay = Ext.Viewport.add({
                            xtype: 'settings',
                            id: 'thissettings',
                            showAnimation: {
                                type: 'popIn',
                                duration: 250,
                                easing: 'ease-out'
                            },
                            hideAnimation: {
                                type: 'popOut',
                                duration: 250,
                                easing: 'ease-out'
                            },
                            centered: true,
                            width: "100%",
                            height: "100%",
                            scrollable: true
                        });
                    }
                this.overlay.show();
                }
            },{
                xtype: 'spacer'
            },{
                xtype: 'button',
                text: 'Logout',
                iconCls: 'x-fa fa-sign-out',
                scope: this,
                handler: function(){
                    Ext.Msg.confirm('Confirm', 'Anda Yakin Ingin Keluar?', this);
                }
            }]
        };
    }
});