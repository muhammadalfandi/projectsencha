/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('MuhammadAlfandi.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.MessageBox',
        'MuhammadAlfandi.view.main.MainController',
        'MuhammadAlfandi.view.main.MainModel',
        'MuhammadAlfandi.view.main.List',
        'MuhammadAlfandi.view.menu.Search',
        'MuhammadAlfandi.view.menu.Cart',
        'MuhammadAlfandi.view.menu.Profile',
        'MuhammadAlfandi.view.menu.Tree',
        'MuhammadAlfandi.view.menu.Audio',
        'MuhammadAlfandi.view.menu.Video'
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [
        {
            title: 'Home',
            iconCls: 'x-fa fa-home',
            layout: 'fit',
            items: [{
                xtype: 'home'
            }]
        },{
            title: 'Search',
            iconCls: 'x-fa fa-search',
            layout: 'fit',
            items: [{
                xtype: 'search'
            }]
        },{
            title: 'Cart',
            iconCls: 'x-fa fa-shopping-cart',
            layout: 'fit',
            items: [{
                xtype: 'cart'
            }]
        },
        {
            title: 'Tree',
            iconCls: 'x-fa fa-sitemap',
            layout: 'fit',
            items: [{
                xtype: 'tree'
            }]
        },
        {
            title: 'Audio',
            iconCls: 'x-fa fa-music',
            layout: 'fit',
            items: [{
                xtype: 'MyAudio1'
            }]
        },
        {
            title: 'Video',
            iconCls: 'x-fa fa-camera',
            layout: 'fit',
            items: [{
                xtype: 'MyVideo1'
            }]
        },
    ]
});
