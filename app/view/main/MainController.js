/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('MuhammadAlfandi.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    onItemSelected: function (sender, record) {
        Ext.Msg.confirm('Confirm', 'Anda Ingin Membeli?', 'onConfirm', this);
    },

    onConfirm: function (choice) {
        if (choice === 'yes') {
            alert('Berhasil Membeli');
        }
        else {
            alert('Anda Tidak Membeli');
        }
    },

    keranjang: function (sender, record) {
        Ext.Msg.confirm('Confirm', 'Anda Yakin Ingin Menambahkan Item Ini?', 'pilih', this);
    },

    pilih: function (choice) {
        if (choice === 'yes') {
            alert('Item Berhasil Ditambahkan');
        }
        else {
            alert('Item Tidak Ditambahkan');
        }
    },

    onEdit: function(){
        if (!Ext.getCmp('thissettings')) {
            this.overlay = Ext.Viewport.add({
                xtype: 'settings',
                id: 'thissettings',
                showAnimation: {
                    type: 'popIn',
                    duration: 250,
                    easing: 'ease-out'
                },
                hideAnimation: {
                    type: 'popOut',
                    duration: 250,
                    easing: 'ease-out'
                },
                centered: true,
                width: "100%",
                height: "100%",
                scrollable: true
            });
        }
    this.overlay.show();
    }
});
