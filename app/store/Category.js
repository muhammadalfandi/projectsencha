Ext.define('MuhammadAlfandi.store.Category', {
    extend: 'Ext.data.Store',
    storeId: 'category',
    alias: 'store.category',

    fields: [
        'kategori', 'image'
    ],

    data: { items: [
        {image: 'foto1.png', kategori : 'Bahan Pokok'},
        {image: 'foto1.png', kategori : 'Minuman'},
        {image: 'mr.jpg', kategori : 'Makanan Ringan'},
        {image: 'rokok.png', kategori : 'Rokok'},
        {image: 'gas.png', kategori : 'Gas'}
    ]},

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
