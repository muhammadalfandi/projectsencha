Ext.define('MuhammadAlfandi.store.Personnel', {
    extend: 'Ext.data.Store',

    alias: 'store.personnel',

    fields: [
        'photo', 'nama', 'harga', 'jumlah', 'status'
    ],

    data: { items: [
    { photo: 'beras.jpg', nama: 'Beras',          harga: "100000", jumlah: "100", status: "Tersedia" },
    { photo: 'teh.jpg', nama: 'Teh Kotak',        harga: "50000",  jumlah: "50",  status: "habis" },
    { photo: 'minyak.jpg', nama: 'Minyak Goreng', harga: "20000",  jumlah: "10",  status: "Tersedia" },
    { photo: 'sirup.jpg', nama: 'Sirup',          harga: "10000",  jumlah: "10",  status: "Habis" }
    ]},

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
